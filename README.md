# zoover test task

You need npm installed for running this app

To get all dependencies run
````bash
npm i
````

### Run app in dev environment with webpack-dev-server

````bash
npm run dev
````

### Run application in prod environment

```bash
npm run build
npm start
```

then open http://localhost:5000 in your browser
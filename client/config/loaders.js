module.exports = {
    /** Pre-loaders */
    "source-map": {
        test: /\.(js|css)$/,
        enforce: "pre",
        loader: "source-map-loader"
    },

    /** Loaders */
    "js": {
        test: /\.(js|jsx)$/,
        loader: {
            loader: "babel-loader",
            options: {
                presets: ["es2015", "react"],
                plugins: [require("babel-plugin-transform-object-rest-spread")]
            }
        },
        exclude: /node_modules/
    },

    "css": {
        test: /\.css$/,
        loader: [
            "style-loader",
            {
                loader: "css-loader",
                options: {
                    sourceMap: true,
                    minimize: true
                }
            }
        ]
    },

    "html": {
        test: /\.html$/,
        loader: "html-loader",
        exclude: [/index\.html$/],
        options: {
            interpolate: "require",
            minimize: true,
            removeAttributeQuotes: false,
            caseSensitive: true,
            root: "scripts/",
            customAttrSurround: [
                [/#/, /(?:)/],
                [/\*/, /(?:)/],
                [/\[?\(?/, /(?:)/]
            ],
            customAttrAssign: [/\)?\]?=/]
        }
    }
};
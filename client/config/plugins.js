const HtmlWebpackPlugin = require("html-webpack-plugin");
const ScriptExtHtmlWebpackPlugin = require("script-ext-html-webpack-plugin");

module.exports = {
    /*
     * Plugin: HtmlWebpackPlugin
     * Description: Simplifies creation of HTML files to serve your webpack bundles.
     * This is especially useful for webpack bundles that include a hash in the filename
     * which changes every compilation.
     *
     * See: https://github.com/ampedandwired/html-webpack-plugin
     */
    "html": new HtmlWebpackPlugin({
        filename: "index.html",
        template: "client/src/index.ejs",
        chunksSortMode: "dependency"
    }),

    "html-async": new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: "async"
    })
};
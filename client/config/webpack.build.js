const helpers = require("./helpers");
const webpack = require("webpack");
const commonConfig = require("./webpack.common");
const loaders = require("./loaders");
const plugins = require("./plugins");

/**
 * Webpack Plugins
 */
const WebpackMd5Hash = require("webpack-md5-hash");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");


module.exports = Object.assign({}, commonConfig, {
    /**
     * Developer tool to enhance debugging
     *
     * See: http://webpack.github.io/docs/configuration.html#devtool
     * See: https://github.com/webpack/docs/wiki/build-performance#sourcemaps
     */
    devtool: "source-map",

    /**
     * Options affecting the output of the compilation.
     *
     * See: http://webpack.github.io/docs/configuration.html#output
     */
    output: {
        /**
         * The output directory as absolute path (required).
         *
         * See: http://webpack.github.io/docs/configuration.html#output-path
         */
        path: helpers.root("dist"),

        /**
         * Specifies the name of each output file on disk.
         * IMPORTANT: You must not specify an absolute path here!
         *
         * See: http://webpack.github.io/docs/configuration.html#output-filename
         */
        filename: "scripts/[name].bundle.js",

        /**
         * The filename of non-entry chunks as relative path
         * inside the output.path directory.
         *
         * See: http://webpack.github.io/docs/configuration.html#output-chunkfilename
         */
        chunkFilename: "scripts/[id].chunk.js"
    },

    /*
     * Options affecting the normal modules.
     *
     * See: http://webpack.github.io/docs/configuration.html#module
     */
    module: {
        rules: Object.keys(loaders).map(loaderName => {
            if (loaderName !== "css") {
                return loaders[loaderName];
            }

            return {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: {
                        loader: "css-loader",
                        options: {
                            sourceMap: true,
                            minimize: true
                        }
                    },
                    publicPath: "../"
                })
            };
        })
    },

    /**
     * Add additional plugins to the compiler.
     *
     * See: http://webpack.github.io/docs/configuration.html#plugins
     */
    plugins: Object.keys(plugins).map(pluginName => plugins[pluginName]).concat([
        /**
         * Plugin: WebpackMd5Hash
         * Description: Plugin to replace a standard webpack chunkhash with md5.
         *
         * See: https://www.npmjs.com/package/webpack-md5-hash
         */
        new WebpackMd5Hash(),

        /**
         * Plugin: ExtractTextPlugin
         * Description: Extracts styles in separate chunks
         *
         * See: https://github.com/webpack-contrib/extract-text-webpack-plugin
         */
        new ExtractTextPlugin({
            filename: "styles/[name].bundle.css",
            allChunks: true
        }),

        /**
         * Plugin: UglifyJSPlugin
         * Description: Uses uglify.js for compression sources
         *
         * See: https://webpack.js.org/plugins/uglifyjs-webpack-plugin/
         */
        new UglifyJSPlugin({
            sourceMap: true,
            compress: {
                warnings: false,
                unused: false // Keep used code so that the plugin can accidentally remove needed code
            }
        })
    ])
});
const webpack = require("webpack");
const helpers = require("./helpers");

/*
 * Webpack configuration
 *
 * See: http://webpack.github.io/docs/configuration.html#cli
 */
module.exports = {
    /*
     * The entry point for the bundle
     * Our Angular.js app
     *
     * See: http://webpack.github.io/docs/configuration.html#entry
     */
    entry: {
        "app": helpers.root("src/main.jsx")
    },

    /*
     * Options affecting the resolving of modules.
     *
     * See: http://webpack.github.io/docs/configuration.html#resolve
     */
    resolve: {
        extensions: [".jsx", ".js"],

        modules: [
            helpers.root("src"),
            helpers.root("../node_modules")
        ]
    },

    /*
     * Include polyfills or mocks for various node stuff
     * Description: Node configuration
     *
     * See: https://webpack.github.io/docs/configuration.html#node
     */
    node: {
        global: true,
        crypto: "empty",
        module: false,
        clearImmediate: false,
        setImmediate: false
    }
};

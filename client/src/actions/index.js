export const REVIEWS_FETCH_REQUESTED = "REVIEWS_FETCH_REQUESTED";
export const REVIEWS_FETCH_SUCCEEDED = "REVIEWS_FETCH_SUCCEEDED";
export const REVIEWS_FETCH_FAILED = "REVIEWS_FETCH_FAILED";

export const REVIEWS_FETCH_MORE = "REVIEWS_FETCH_MORE";

export const REVIEWS_STAT_FETCH_REQUESTED = "REVIEWS_STAT_FETCH_REQUESTED";
export const REVIEWS_STAT_FETCH_SUCCEEDED = "REVIEWS_STAT_FETCH_SUCCEEDED";
export const REVIEWS_STAT_FETCH_FAILED = "REVIEWS_STAT_FETCH_FAILED";

export function requestReviews() {
    return {
        type: REVIEWS_FETCH_REQUESTED
    };
}

export function requestReviewsStat() {
    return {
        type: REVIEWS_STAT_FETCH_REQUESTED
    };
}

export function loadMoreReviews(nextPageUrl) {
    return {
        type: REVIEWS_FETCH_MORE,
        nextPageUrl
    };
}

export const CALC_TRAVELED_WITH_VALUES = "CALC_TRAVELED_WITH_VALUES";

export function calcTraveledWithValues(reviews) {
    return {
        type: CALC_TRAVELED_WITH_VALUES,
        reviews
    };
}

export const SET_SORTING_ATTRIBUTE = "SET_SORTING_ATTRIBUTE";

export function setSortingAttribute(attribute) {
    return {
        type: SET_SORTING_ATTRIBUTE,
        attribute
    };
}

export const SET_SORTING_DIRECTION = "SET_SORTING_DIRECTION";

export function setSortingDirection(isAscending) {
    return {
        type: SET_SORTING_DIRECTION,
        isAscending
    };
}

export const REVERT_SORTING_DIRECTION = "REVERT_SORTING_DIRECTION";

export function revertSortingDirection() {
    return {
        type: REVERT_SORTING_DIRECTION
    };
}

export const SET_TRAVELED_WITH_FILTER = "SET_TRAVELED_WITH_FILTER";

export function setTraveledWithFilter(text) {
    return {
        type: SET_TRAVELED_WITH_FILTER,
        text
    };
}
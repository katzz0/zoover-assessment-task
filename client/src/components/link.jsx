import "./link.css";
import React, {PropTypes} from "react";

const Link  = ({className, children, onClick}) => {
    return (
        <a
            className={"link " + className}
            href="#"
            onClick={(e) => {
                e.preventDefault();
                onClick();
            }}
        >
            {children}
        </a>
    );
}


Link.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func.isRequired
};

export default Link;
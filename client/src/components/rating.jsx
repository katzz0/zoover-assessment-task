import React, {PropTypes} from "react";

const Rating = ({name, value}) => (
    <dl>
        <dt>{name.replace(/([A-Z])/g, " $1").replace(/^./, s => s.toUpperCase())}</dt>
        <dd>{Math.round(value * 10) / 10}</dd>
    </dl>
);

Rating.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
};

export default Rating;
import "./review.css";
import React, {PropTypes} from "react";

const Review = ({title, text, user, entryDate, travelDate, traveledWith}) => (
    <li className="review">
        <div className="title">
            <h3>{title}</h3>
            <span className="entry-date">Entry date: {new Date(entryDate).toLocaleDateString()}</span> /&nbsp;
            <span className="travel-date">Travel date: {new Date(travelDate).toLocaleDateString()}</span>
        </div>
        <div className="text">
            {text}
        </div>
        <div className="additional-info">
            <span className="user">{user}</span>

            <span className="traveled-with">Traveled with: {traveledWith}</span>
        </div>
    </li>
);

Review.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    entryDate: PropTypes.number.isRequired,
    travelDate: PropTypes.number.isRequired,
    traveledWith: PropTypes.string.isRequired
};

export default Review;

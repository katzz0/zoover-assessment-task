import "./reviews-list.css";
import React, {PropTypes} from "react";
import Review from "./review";

const ReviewsList = ({reviews, errorMessage}) => (
    <ul className="reviews-list">
        {errorMessage && <div className="error">{errorMessage}</div>}
        {!errorMessage && reviews.map(review => (
            <Review key={review.id} {...review} />
        ))}
    </ul>
);

ReviewsList.propTypes = {
    errorMessage: PropTypes.string,
    reviews: PropTypes.arrayOf(
        PropTypes.shape({
            title: PropTypes.string,
            text: PropTypes.string.isRequired,
            user: PropTypes.string.isRequired,
            entryDate: PropTypes.number.isRequired,
            travelDate: PropTypes.number.isRequired,
            traveledWith: PropTypes.string.isRequired
        }).isRequired
    ).isRequired,
};

export default ReviewsList;

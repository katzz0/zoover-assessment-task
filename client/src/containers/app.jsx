import "./app.css";

import React, {Component, PropTypes} from "react";
import {connect} from "react-redux";
import Rating from "../components/rating";
import Link from "../components/link";
import SortLink from "./sort-link";
import Filter from "./filter";
import FilteredReviewsList from "./filtered-reviews-list";
import {requestReviews, requestReviewsStat, loadMoreReviews} from "../actions";

class App extends Component {
    constructor(props) {
        super(props);

        const {dispatch} = props;
        dispatch(requestReviews());
        dispatch(requestReviewsStat());
    }

    render() {
        const {dispatch, isFetching, stat, lastUpdated, hasMoreReviews, reviewsNextPageUrl, filter} = this.props;

        return (
            <div>
                {stat.errorMessage && <div className="error">{stat.errorMessage}</div>}
                {!stat.errorMessage &&
                    <div className="average">
                        <div className="mark-general">
                            <span>{!isFetching && stat.general !== undefined && Math.round(stat.general * 10) / 10}</span>
                        </div>
                        <div className="mark-aspects">
                            {Object.keys(stat.aspects).map(name =>
                                <Rating key={name} name={name} value={stat.aspects[name]} />
                            )}
                        </div>
                        <div className="traveled-with">
                            People in average traveled with {stat.traveledWith}
                        </div>
                        <span className="last-updated">
                            {lastUpdated &&
                                <span>Last updated at {new Date(lastUpdated).toLocaleTimeString()}.{" "}</span>
                            }
                        </span>
                    </div>
                }

                <div className="filter-sort">
                    <div className="col1"><Filter dispatch={dispatch} traveledWithValues={filter.traveledWithValues} /></div>
                    <div className="col2">
                        Sort by:
                        <SortLink attribute="entryDate">Entry date</SortLink>
                        <SortLink attribute="travelDate">Travel date</SortLink>
                    </div>
                </div>
                <FilteredReviewsList />

                {hasMoreReviews &&
                    <Link className="load-more" onClick={() => {dispatch(loadMoreReviews(reviewsNextPageUrl))}}>
                        Load more reviews
                    </Link>
                }
            </div>
        )
    }
}

App.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    stat: PropTypes.object,
    reviews: PropTypes.array,
    lastUpdated: PropTypes.number,
    dispatch: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    const {stat, reviews, filter} = state;

    return {
        isFetching: reviews.isFetching || stat.isFetching,
        stat,
        lastUpdated: reviews.lastUpdated,
        hasMoreReviews: reviews.hasMoreReviews,
        reviewsNextPageUrl: reviews.nextPageUrl,
        filter
    };
}

export default connect(mapStateToProps)(App);
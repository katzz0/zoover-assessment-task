import "./filter.css";
import React, {PropTypes, Component} from "react";
import {setTraveledWithFilter} from "../actions/index";

class Filter extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const {dispatch} = this.props;

        dispatch(setTraveledWithFilter(event.target.value));
    }

    render() {
        return (
            <div className="filter">
                <label>
                    Filter by traveled with:
                    <select onChange={this.handleChange}>
                        <option value=""></option>
                        {this.props.traveledWithValues.map((value, i) =>
                            <option key={i + value} value={value}>{value}</option>
                        )}
                    </select>
                </label>
            </div>
        );
    }
}

Filter.propTypes = {
    dispatch: PropTypes.func.isRequired,
    traveledWithValues: PropTypes.arrayOf(PropTypes.string)
};

export default Filter;
import {connect} from "react-redux";
import ReviewsList from "../components/reviews-list";

const getFilteredReviews = (reviews, travelWithFilter) => {
    return reviews.filter(review => travelWithFilter ?
        review.traveledWith.toLocaleLowerCase().startsWith(travelWithFilter.toLocaleLowerCase()) :
        true
    );
};

const sortReviews = (reviews, attribute, isAsc) => {
    if (!attribute) {
        return reviews;
    }

    return reviews.sort((a, b) => {
        if (a[attribute] > b[attribute]) {
            return isAsc ? 1 : -1;
        } else if (a[attribute] < b[attribute]) {
            return isAsc ? -1 : 1;
        }

        return 0;
    });
};

const mapStateToProps = state => {
    return {
        reviews: sortReviews(
            getFilteredReviews(state.reviews.items, state.filter.traveledWithFilter),
            state.sort.reviewsSortAttr,
            state.sort.reviewsSortAscending
        ),
        errorMessage: state.reviews.errorMessage
    }
};

const FilteredReviewsList = connect(
    mapStateToProps
)(ReviewsList);

export default FilteredReviewsList;
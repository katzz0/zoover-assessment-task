import {connect} from "react-redux";
import {setSortingAttribute, revertSortingDirection} from "../actions/index";
import Link from "../components/link";

const mapStateToProps = (state, ownProps) => {
    return {
        className: state.sort.reviewsSortAttr === ownProps.attribute ?
            (state.sort.reviewsSortAscending ? "asc" : "desc") :
            ""
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onClick: () => {
            dispatch(setSortingAttribute(ownProps.attribute));
            dispatch(revertSortingDirection());
        }
    };
};

const SortLink = connect(
    mapStateToProps,
    mapDispatchToProps
)(Link);

export default SortLink;
import * as ActionTypes from "../actions";
import {combineReducers} from "redux";

function reviews(state = {
    isFetching: false,
    items: [],
    lastUpdated: undefined,
    errorMessage: undefined,
    hasMoreReviews: true,
    nextPageUrl: undefined
}, action) {
    switch (action.type) {
        case ActionTypes.REVIEWS_FETCH_REQUESTED:
            return {...state, isFetching: true};

        case ActionTypes.REVIEWS_FETCH_SUCCEEDED:
            return {
                ...state,
                isFetching: false,
                items: state.items.slice().concat(action.reviews.response.reviews.map(review => {
                    const locale = review.locale;

                    return Object.assign({}, review, {
                        title: review.titles[locale],
                        text: review.texts[locale]
                    });
                })),
                lastUpdated: new Date().getTime(),
                hasMoreReviews: action.reviews.response.restCount > 0,
                nextPageUrl: action.reviews.response.nextPageUrl
            };

        case ActionTypes.REVIEWS_FETCH_FAILED:
            return {
                ...state,
                errorMessage: action.message
            };

        default:
            return state;
    }
}

function stat(state = {
    isFetching: false,
    general: undefined,
    aspects: {},
    traveledWith: undefined,
    lastUpdated: undefined,
    errorMessage: undefined
}, action) {
    switch (action.type) {
        case ActionTypes.REVIEWS_STAT_FETCH_REQUESTED:
            return {...state, isFetching: true};

        case ActionTypes.REVIEWS_STAT_FETCH_SUCCEEDED:
            return Object.assign({
                isFetching: false,
                lastUpdated: new Date().getTime()
            }, action.reviews.response.stat);

        case ActionTypes.REVIEWS_STAT_FETCH_FAILED:
            return {
                ...state,
                errorMessage: action.message
            };

        default:
            return state;
    }
}

function filter(state = {
    traveledWithFilter: undefined,
    traveledWithValues: []
}, action) {
    switch (action.type) {
        case ActionTypes.SET_TRAVELED_WITH_FILTER:
            return {
                ...state,
                traveledWithFilter: action.text || undefined
            };

        case ActionTypes.CALC_TRAVELED_WITH_VALUES:
            return {
                ...state,
                traveledWithValues: state.traveledWithValues.concat(
                    Object.keys(action.reviews.reduce((values, review) => {
                        values[review.traveledWith] = true;
                        return values;
                    }, {})).filter(value => state.traveledWithValues.indexOf(value) === -1)
                )
            };

        default:
            return state;
    }
}

function sort(state = {
    reviewsSortAttr: undefined,
    reviewsSortAscending: true
}, action) {
    switch (action.type) {
        case ActionTypes.SET_SORTING_ATTRIBUTE:
            return {
                ...state,
                reviewsSortAttr: action.attribute || undefined
            };

        case ActionTypes.SET_SORTING_DIRECTION:
            return {
                ...state,
                reviewsSortAscending: !!action.isAscending
            };

        case ActionTypes.REVERT_SORTING_DIRECTION:
            return {
                ...state,
                reviewsSortAscending: !state.reviewsSortAscending
            };

        default:
            return state;
    }
}

const rootReducer = combineReducers({
    reviews,
    stat,
    filter,
    sort
});

export default rootReducer;
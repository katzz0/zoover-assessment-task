import {put, call, takeLatest} from "redux-saga/effects";
import * as api from "../services";
import * as ActionTypes from "../actions";

function* fetchReviews() {
    try {
        const reviews = yield call(api.fetchReviews);
        yield put({type: ActionTypes.REVIEWS_FETCH_SUCCEEDED, reviews});
    } catch (e) {
        yield put({type: ActionTypes.REVIEWS_FETCH_FAILED, message: e.error});
    }
}

function* fetchMoreReviews(action) {
    try {
        const reviews = yield call(api.fetchMoreReviews, action.nextPageUrl);
        yield put({type: ActionTypes.REVIEWS_FETCH_SUCCEEDED, reviews});
    } catch (e) {
        yield put({type: ActionTypes.REVIEWS_FETCH_FAILED, message: e.error});
    }
}

function* fetchReviewsStat() {
    try {
        const reviews = yield call(api.fetchReviewsStat);
        yield put({type: ActionTypes.REVIEWS_STAT_FETCH_SUCCEEDED, reviews});
    } catch (e) {
        yield put({type: ActionTypes.REVIEWS_STAT_FETCH_FAILED, message: e.error});
    }
}

function* refreshFilter(action) {
    yield put({type: ActionTypes.CALC_TRAVELED_WITH_VALUES, reviews: action.reviews.response.reviews});
}

export default function* rootSaga() {
    yield takeLatest(ActionTypes.REVIEWS_STAT_FETCH_REQUESTED, fetchReviewsStat);
    yield takeLatest(ActionTypes.REVIEWS_FETCH_REQUESTED, fetchReviews);
    yield takeLatest(ActionTypes.REVIEWS_FETCH_MORE, fetchMoreReviews);
    yield takeLatest(ActionTypes.REVIEWS_FETCH_SUCCEEDED, refreshFilter);
}
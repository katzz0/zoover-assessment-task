import "isomorphic-fetch";

// Extracts the next page URL from Github API response.
function getNextPageUrl(response) {
   return response.headers.get("nextPage");
}

// Extracts the rest count of entities.
function getRestCountOfEntities(response) {
    return response.headers.get("restCount");
}

const API_ROOT = 'http://localhost:5000/';

function callApi(endpoint) {
    const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint;

    try {
        return fetch(fullUrl).then(response => {
            if (!response.ok) {
                return Promise.reject();
            }

            return response.json().then(json => ({json, response}));
        }).then(({json, response}) => {
            const nextPageUrl = getNextPageUrl(response);
            const restCount = getRestCountOfEntities(response);
            return Object.assign({}, json, {nextPageUrl, restCount});
        }).then(
            response => ({response}),
            error => Promise.reject({error: error && error.message || "Something bad happened"})
        );
    } catch (e) {
        return Promise.resolve({error: "Something bad happened"});
    }

}

// api services
export const fetchReviews = () => callApi(`reviews`);
export const fetchMoreReviews = (url) => callApi(url);
export const fetchReviewsStat = () => callApi(`stat`);
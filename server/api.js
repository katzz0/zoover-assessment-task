const express = require("express");
const reviewsProvider = require("./data-providers/reviews");
const reviewsAverageProvider = require("./data-providers/reviews-average");

const app = express();

const PAGE_SIZE = 5;

app.get("/stat", (req, res) => {
    reviewsProvider()(reviews => {
        reviewsAverageProvider(reviews)(stat => {
            res.json({stat});
        });
    });
});

app.get("/reviews/:pageNumber*?", (req, res) => {
    const pageNumber = req.params.pageNumber > 0 ? req.params.pageNumber : 1;

    reviewsProvider()(reviews => {
        const restReviewsCount = reviews.length - pageNumber * PAGE_SIZE;

        res.set("nextPage", `reviews/${+pageNumber + 1}`);
        res.set("restCount", restReviewsCount >= 0 ? restReviewsCount : 0);
        res.json({reviews: reviews.slice((pageNumber - 1) * PAGE_SIZE, pageNumber * PAGE_SIZE)});
    });
});

module.exports = app;
/**
 * Finds a closest value in the array
 */
function closest(searchValue, arr) {
    let result = arr[0],
        diff = Infinity;

    for (let value of arr) {
        const newDiff = Math.abs(searchValue - value);
        if (newDiff < diff) {
            diff = newDiff;
            result = value;
        }
    }

    return result;
}

/**
 * Calculates sums of ratings and travel with counts
 */
function calcSums(reviews) {
    const currentYear = new Date().getFullYear(),
        result = {
            general: 0,
            aspects: {},
            traveledWith: {},
            amount: 0
        };

    reviews.forEach(review => {
        const general = review["ratings"]["general"]["general"],
            aspects = review["ratings"]["aspects"],
            traveledWith = review["traveledWith"];

        const reviewAge = currentYear - new Date(review["travelDate"]).getFullYear(),
            weight = reviewAge > 5 ? 0.5 : 1 - reviewAge * 0.1;

        result.general += general * weight;

        if (!result.aspects) {
            result.aspects = aspects;
        } else {
            Object.keys(aspects).forEach(ratingName => {
                if (!result.aspects[ratingName]) {
                    result.aspects[ratingName] = aspects[ratingName] * weight;
                } else {
                    result.aspects[ratingName] += aspects[ratingName] * weight;
                }
            });
        }

        if (!result.traveledWith[traveledWith]) {
            result.traveledWith[traveledWith] = 1;
        } else {
            result.traveledWith[traveledWith]++;
        }

        result.amount++;
    });

    return result;
}

module.exports = function calcReviewsAveragesStat(reviews) {
    return function (func) {
        const result = {
            general: 0,
            aspects: {},
            traveledWith: ""
        };

        const sums = calcSums(reviews);

        result.general = sums.amount > 0 ? sums.general / sums.amount : 0;
        Object.keys(sums.aspects).forEach(ratingName => {
            result.aspects[ratingName] = sums.amount ? sums.aspects[ratingName] / sums.amount : 0;
        });

        const traveledWithHashByRating = {};

        const averageTravelWithAmount = Object.keys(sums.traveledWith).reduce((sum, travelWithCaption) => {
                traveledWithHashByRating[sums.traveledWith[travelWithCaption]] = travelWithCaption;
                return sum + sums.traveledWith[travelWithCaption];
            }, 0) / Object.keys(sums.traveledWith).length;

        result.traveledWith = traveledWithHashByRating[closest(
            averageTravelWithAmount,
            Object.keys(sums.traveledWith).map(travelWithCaption => sums.traveledWith[travelWithCaption])
        )];

        func(result);
    }
};
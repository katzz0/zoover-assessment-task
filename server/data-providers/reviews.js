const fs = require("fs");

module.exports = function fetchReviews() {
    return function (func) {
        // It could be an async operation, so we need an opportunity to the future
        func(JSON.parse(fs.readFileSync("data/reviews.json", "utf8")));
    };
};
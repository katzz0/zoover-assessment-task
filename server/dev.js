const webpack = require("webpack");
const webpackDevMiddleware = require("webpack-dev-middleware");
const webpackHotMiddleware = require("webpack-hot-middleware");
const webpackConfig = require("../client/config/webpack.dev");
const app = require("./api");

const PORT = process.env.PORT || 5000;

const compiler = webpack(webpackConfig);
app.use(webpackDevMiddleware(compiler, {
    noInfo: false,
    publicPath: webpackConfig.output.publicPath,
    lazy: false,
    stats: {
        colors: true
    }
}));
app.use(webpackHotMiddleware(compiler));

app.listen(PORT, () => {
    console.info(`The server is running on port ${PORT}.`)
});
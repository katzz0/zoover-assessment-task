const express = require("express");
const app = require("./api");

const PORT = process.env.PORT || 5000;

app.use(express.static("client/dist", {
    dotfiles: "ignore",
    etag: false,
    index: "index.html",
    redirect: false
}));

app.listen(PORT, () => {
    console.info(`The server is running on port ${PORT}.`)
});